// Output 1
db.fruits.aggregate([
	{$match : {onSale : true}},
	{$count : "fruitsOnSale"}
]);

// Output 2
db.fruits.aggregate([
	{$match : {stock : {$gte : 20}}},
	{$count : "enoughStocks"}
]);

// Output 3
db.fruits.aggregate([
	{$match : {onSale: true}},
	{$group : {_id : "$supplier_id", avg_price: {$avg : "$price"}}}
]);

// Output 4
db.fruits.aggregate([
	{$match: {onSale : true}},
	{$group: {_id : "$supplier_id", max_price: {$max: "$price"}}}
]);

// Output 5
db.fruits.aggregate([
	{$match: {onSale : true}},
	{$group: {_id : "$supplier_id", min_price: {$min: "$price"}}}
]);